local status_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status_ok then
	return
end

local lspconfig = require("lspconfig")

local servers = { "jsonls", "lua-language-server", "pyright", "astro", "tailwindcss" }

lsp_installer.setup({
	ensure_installed = servers,
})

for _, server in pairs(servers) do
	local opts = {
		on_attach = require("user.lsp.handlers").on_attach,
		capabilities = require("user.lsp.handlers").capabilities,
	}
	local has_custom_opts, server_custom_opts = pcall(require, "user.lsp.settings." .. server)
	if has_custom_opts then
		opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
	end
	lspconfig[server].setup(opts)
end

-- bet we can invoke current version of ts with nvm which current` here, then based on that change the string
-- stupid fucking shit:
-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#volar
require("lspconfig").volar.setup({
	on_attach = require("user.lsp.handlers").on_attach,
	capabilities = require("user.lsp.handlers").capabilities,
	flags = {
		debounce_text_changes = 150,
	},
	-- Enable "Take Over Mode" where volar will provide all TS LSP services
	-- This drastically improves the responsiveness of diagnostic updates on change
	filetypes = { "typescript", "javascript", "javascriptreact", "typescriptreact", "vue", "json", "nuxt" },
	init_options = {
		typescript = {
			-- serverPath = '/path/to/.npm/lib/node_modules/typescript/lib/tsserverlib.js'
			--[[ serverPath = "/home/bes/.nvm/versions/node/v16.16.0/lib/node_modules/typescript/lib/tsserverlibrary.js", ]]
			-- Alternative location if installed as root:
			--[[ serverPath = "/usr/local/lib/node_modules/typescript/lib/tsserverlibrary.js", ]]
			tsdk = "/home/bes/.npm-packages/lib/node_modules/typescript/lib",
		},
	},
})
